﻿using System.Collections.Generic;
using UnityEngine;

public class UnlockSelector : ClickSelector
{
    public static Dictionary<string, bool> LockedItems = new Dictionary<string, bool>();
    public GameObject target;

    protected override void ActivateButton()
    {
        if(!LockedItems.ContainsKey(target.name))
            LockedItems.Add(target.name, true);
        else
        {
            LockedItems[target.name] = true;
        }
    }
}