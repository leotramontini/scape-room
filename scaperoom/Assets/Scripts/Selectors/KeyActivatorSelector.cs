﻿using UnityEngine;

public class KeyActivatorSelector : ClickSelector
{
    public static bool HasKey = false;
    public GameObject target;

    protected override void ActivateButton()
    {
        System.Console.Out.WriteLine($"Você {(HasKey ? "" : "não ")}tem a chave");
        if (HasKey)
        {
            target.SetActive(false);
        }
    }
}