﻿using UnityEngine;
using UnityEngine.UI;

//Usada para ativar e desativar um objeto a partir do clique nele
public class ActivatorSelector : ClickSelector
{
    public GameObject target;
    [Tooltip("Use to dinamically find target")]
    public string targetName;
    [Tooltip("True to activate and false do deactivate target")]
    public bool willActivate;
    [Tooltip("True to enable undo")]
    public bool rollBack = false;
    bool timeToRollback = false;
    public string texto;
    public string lockedTexto;
    void Start()
    {
        if (target == null)
            target = GameObject.Find(targetName);

        if (!UnlockSelector.LockedItems.ContainsKey(m_InteractiveItem.name))
        {
            UnlockSelector.LockedItems.Add(m_InteractiveItem.name, string.IsNullOrEmpty(lockedTexto));
        }
    }

    protected override void ActivateButton()
    {
        target.SetActive(rollBack ? (timeToRollback ? !willActivate : willActivate) : willActivate);
        var x = target.transform.GetChild(0).GetComponent<Text>();
        if (x != null)
        {
            if (!UnlockSelector.LockedItems[m_InteractiveItem.name])
            {
                x.text = lockedTexto.Replace("\\n", "\n");
            }
            else
            {
                x.text = texto.Replace("\\n", "\n"); ;
            }
        }
        timeToRollback = !timeToRollback;
    }

    public void SetTargetName(string s)
    {
        targetName = s;
    }
}

