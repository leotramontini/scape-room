﻿using UnityEngine;

public class KeyFoundSelector : ClickSelector
{
    public static bool HasKey = false;

    protected override void ActivateButton()
    {
        var li = UnlockSelector.LockedItems;
        var x = m_InteractiveItem.name;
        if (li.ContainsKey(x) && li[x])
            KeyActivatorSelector.HasKey = true;
    }
}