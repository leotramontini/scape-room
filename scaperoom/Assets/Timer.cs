﻿using System;
using TMPro;
using UnityEngine;
public class Timer : MonoBehaviour
{
    public event EventHandler GameOver;
    public TextMeshProUGUI text;
    public float Segundos = 300;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Segundos -= Time.deltaTime;
        if(Segundos <= 0)
        {
            GameOver.Invoke(this, EventArgs.Empty);
        }
        text.text = string.Format("{0:mm\\:ss}", TimeSpan.FromSeconds(Segundos));
    }
}
